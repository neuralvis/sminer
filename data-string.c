/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-string.h"

DataString::DataString( const char *field ) : Data( field, DATATYPE_STRING ) {
   
}

Data *DataString::make( const char *field, const YAML::Node &config ) {
    return( new DataString( field ) );
}

void DataString::_clear( void ) {
}

int DataString::parse( const char *text, size_t len ) {

    if ( len > 0 ) {
        _value.assign( text, len );
        _set = true;
    }

    return( 0 );
}

/* END OF FILE */
