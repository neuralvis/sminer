/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _LOG_H_
#define _LOG_H_

#include    "mylib.h"

class Log {

    public:
        
        static void configure( YAML::Node definitions );

        void error( const char *fmt, ... );
        void info( const char *fmt, ... );
        void alert( const char *fmt, ... );
        void debug( const char *fmt, ... );

        Log();

    protected:

    private:

        FILE    *_fp;
        char    *_fname;
        bool    _stdout;

};

extern Log logger;

#endif
