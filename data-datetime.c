/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-datetime.h"

DataDateTime::DataDateTime( const char *field ) : Data( field, DATATYPE_DATETIME ) {
    
}

Data *DataDateTime::make( const char *field, const YAML::Node &config ) {
    return( new DataDateTime( field ) );
}

void DataDateTime::_clear( void ) {
    _time = 0;
}

int DataDateTime::parse(  const char *text, size_t len ) {
    
    _value.assign( text, len );

    if ( _value == "Unknown" ) {
        /* ignore unknown */
        return( 0 );
    }

    /* create an "epoch seconds" from the slurm time */

    struct tm tm;
    bzero( &tm, sizeof( tm ));

    if ( strptime( _value.c_str(), "%Y-%m-%dT%H:%M:%S", &tm ) == NULL ) {
        printf("Failed to translate\n");
        throw( 23 );
    }

    _time = mktime( &tm );

    /* and save the time in slurm text form (replacing the 'T' to make it
     * compatible with postgres */

    _value.replace( 10, 1, " ");

    _set = true;
    return( 0 );
}

time_t DataDateTime::epochSeconds( void ) {
    return( _time );
}

/* END OF FILE */
