/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _SACCT_HEADER_H_
#define _SACCT_HEADER_H_ 

#include    "mylib.h"

class Data;
class DataJobID;
class DataDateTime;
class DataTRES;
class DataNodeList;

class Parser {

    public:

        Parser();
        ~Parser();

        void label( const char *text, size_t len );
        void validate( void );
        void reset( void );
        bool isset( void );

        void data( const char *text, size_t len );

        void clear( void );

        const char *job( void );

        bool shortJob( void );

        void line( const char *line, void (Parser::*cb)( const char *text, size_t len ) );

        void debug( int value );

    private:

        findex_t    _nfields;
        Data        **_map;

        findex_t    _index;

        DataJobID       *_job_id;
        DataDateTime    *_time_submit;
        DataDateTime    *_time_start;
        DataDateTime    *_time_end;
        DataTRES        *_alloc_tres;
        DataNodeList    *_node_list;

        int             _debug;

};

#endif
