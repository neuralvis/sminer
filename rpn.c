/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "rpn.h"

RPN::RPN() {
    _estack_size = 0;
    _stack_size = 0;
}

RPN::~RPN() {

}

/*
 * FUNCTION:    compile
 *
 * PARAMS:      const char * - expression to compile
 *
 * RETURNS:     void
 *
 * THROWS:      AppFatal on compilation error
 *
 * WHATITDOES:  Compiles the RPN expression so that
 *              it can be quickly applied to an arbitrary
 *              value.
 */
void RPN::compile( const char *text ) {

    /* leave room on the stack for the initial value (to be added later) */

    _estack_size = 0;

    const char *ptr = text;

    /* convert the expression into a stack of values and callbacks */

    while ( *ptr ) {

        if ( isspace( *ptr ) ) {
            ptr ++;
            continue;
        } else if ( _estack_size >= RPN_STACK_SIZE ) {
            throw( AppFatal("RPN expression exceeds stack size (%d)", RPN_STACK_SIZE ) );
        }

        _estack[ _estack_size ].value = 0;
        _estack[ _estack_size ].op = NULL;

        if ( isdigit( *ptr ) ) {

            rpn_t value = 0;

            for ( ; isdigit( *ptr ) ; ptr ++ ) {
                value = ( value * 10 ) + ( *ptr - '0' );
            }
            _estack[ _estack_size ++ ].value = value;

        } else {

            char ch = *ptr ++;
 
            if ( ch == '+' ) {
                _estack[ _estack_size ++ ].op = &RPN::op_add;
            } else if ( ch == '-' ) {
                _estack[ _estack_size ++ ].op = &RPN::op_subtract;
            } else if ( ch == '/' ) {
                _estack[ _estack_size ++ ].op = &RPN::op_divide;
            } else if ( ch == '*' ) {
                _estack[ _estack_size ++ ].op = &RPN::op_multiply;
            } else {
                throw( AppFatal( "RPN expresion '%s' has invalid character '%c'", text, ch ) );
            }

        }
  
    }

}

rpn_t RPN::pop( void ) {
    if ( _stack_size == 0 ) {
        throw( AppFatal("RPN attept to POP off empty stack" ) );
    }
    _stack_size --;
    rpn_t value = _stack[ _stack_size ];
    return( value );
}

void RPN::push( rpn_t value ) {
    if ( _stack_size == RPN_STACK_SIZE ) {
        throw( AppFatal("RPN attept to PUSH onto full stack" ) );
    }
    _stack[ _stack_size ++ ] = value;
}

void RPN::op_add( void ) {
    /* pop two values from the stack, add then and push */
    if ( _stack_size < 2 ) {
        throw( AppFatal("RPN attempt to POP two values for + failed" ) );
    }
    rpn_t v1 = pop();
    rpn_t v2 = pop();
    push( v2 + v1 );
}

void RPN::op_subtract( void ) {
    if ( _stack_size < 2 ) {
        throw( AppFatal("RPN attempt to POP two values for + failed" ) );
    }
    rpn_t v1 = pop();
    rpn_t v2 = pop();
    push( v2 - v1 );
}

void RPN::op_multiply( void ) {
    if ( _stack_size < 2 ) {
        throw( AppFatal("RPN attempt to POP two values for + failed" ) );
    }
    rpn_t v1 = pop();
    rpn_t v2 = pop();
    push( v2 * v1 );
}

void RPN::op_divide( void ) {
    if ( _stack_size < 2 ) {
        throw( AppFatal("RPN attempt to POP two values for + failed" ) );
    }
    rpn_t v1 = pop();
    rpn_t v2 = pop();
    push( v2 / v1 );
}

rpn_t RPN::run( rpn_t value ) {

    _stack_size = 0;

    push( value );

    for ( int index = 0 ; index < _estack_size ; index ++ ) {

        if ( _estack[ index ].op == NULL ) {
            push( _estack[ index ].value );
        } else {
            (this->*_estack[ index ].op)();
        }

    }

    if ( _stack_size != 1 ) {
        throw( AppFatal("RPN expected only one value on the stack - found %d", _stack_size ) );
    }

    return( _stack[ 0 ] );
}

bool RPN::defined( void ) {
    return( _estack_size > 0 ? true : false );
}

/* END OF FILE */
