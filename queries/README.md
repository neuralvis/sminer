# Example SQL queries

These queries work with the example schema.

## For the 2020 calendar year list all job states with job count:

```
SELECT
    state, COUNT(state) AS count
  FROM
    slurm_job
  WHERE
    time_start < '2021-01-01' AND time_end >= '2020-01-01'
  GROUP BY
    state
  ORDER BY
    count DESC;
```

## For all loaded jobs list the top 10 most frequently used nodes:

```
SELECT
    node_name,count(node_name) AS count
  FROM
    slurm_job_node_link
  GROUP BY
    node_name
  ORDER BY
    count DESC
  LIMIT 10;
```

## Show the 80th percentile for job time to launch from submission

```
SELECT
    percentile_disc(0.8)
  WITHIN GROUP
    ( ORDER BY EXTRACT( epoch from (time_start - time_submit)) )
  FROM
    slurm_job;
```

## Show the top 10 users by job submission for 2020:

_This is quite similar to the state query above_.

```
SELECT
    state, COUNT(state) AS count
  FROM
    slurm_job
  WHERE
    time_start < '2021-01-01' AND time_end >= '2020-01-01'
  GROUP BY
    state
  ORDER BY
    count DESC;
```

## Show the top 10 GPU users by job submission for 2020:

```
SELECT
    state, COUNT(state) AS count
  FROM
    slurm_job
  WHERE
    time_start < '2021-01-01' AND time_end >= '2020-01-01' AND tres_gpu > 0
  GROUP BY
    state
  ORDER BY
    count DESC;
```

## Show the top 10 most popular GPU nodes by job launch in 2020:

```
SELECT
    node_name, COUNT(node_name) AS count
  FROM
    slurm_job as j, slurm_job_node_link as l
  WHERE
    j.job_index = l.job_index AND
    time_start < '2021-01-01' AND
    time_end >= '2020-01-01' AND
    tres_gpu > 0
  GROUP BY
    node_name
  ORDER BY
    count DESC
  LIMIT 10;
```

END.
