/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "db-field.h"
#include    "slurm.h"
#include    "data.h"
#include    "db-table.h"

namespace DB {

    Field::Field( Table *table, const char *name, const char *parser, const char *slurm, const char *references,
            const char *default_value, const char *cast ) {
        _table = table;
        _name = xstrdup( name );
        _name_len = xstrlen( _name );
        _parser = xstrdup( parser );
        _slurm = xstrdup( slurm );
        _ref_text = xstrdup( references );
        _ref_table = NULL;
        _ref_field = NULL;
        _ref_index = -1;
        _default = xstrdup( default_value );
        _cast = xstrdup( cast );
        _data = NULL;
        _data_index = 0;

        zdebug( 0, "field[%s] slurm=%s parser=%s\n", _name, _slurm, _parser );

        if ( _slurm ) {
            try {
                _data = Data::connect( _slurm, &_data_index );
            } catch ( AppExcept &e ) {
                throw( AppExcept("field %s: %s", _name, e.what() ) );
            }
        }

    }

    Field *Field::make( Table *table, const char *name, const char *parser, const char *slurm, const char *references,
            const char *default_value, const char *cast ) {
        return( new Field( table, name, parser, slurm, references, default_value, cast ) );

    }

    /*
     * FUNCTION:    name
     *
     * PARAM:       none
     *
     * RETURNS:     const char * - the database name of the field
     *
     * WHATITDOES:  returns the database name of the field
     */
    const char *Field::name( void ) {
        return( _name );
    }

    /*
     * FUNCTION:    nameLen
     *
     * PARAM:       none
     *
     * RETURNS:     size_t - the length of the database name
     *
     * WHATITDOES:  returns the length of the database name of this field
     */
    size_t Field::nameLen( void ) {
        return( _name_len );
    }

    /*
     * FUNCTION:    cast
     *
     * PARAM:       none
     *
     * RETURNS:     const char * - the database "cast" string for this field
     *
     * WHATITDOES:  returns casting information for the value of this field
     *              to be injected into the database
     */
    const char *Field::cast( void ) {
        return( _cast );
    }

    /*
     * FUNCTION:    fetch
     *
     * PARAM:       none
     *
     * RETURNS:     const char * - value of this field as stored in a Data object
     *
     * WHATITDOES:  queries the connected data object and returns the value;
     *              If the Data object doesn't have a stored value then NULL
     *              is returned.
     */

    const char *Field::fetch( void ) {

        if (( _data )&&( _data->isset() )) {
                return( _data->value( _data_index ) );
        } else {
            return( NULL );
        }

    }

    /*
     * FUNCTION:    type
     *
     * PARAM:       none
     *
     * RETURNS:     DataType - ID of the connected Data object
     *
     * WHATITDOES:  Passes the query to the connected Data object
     *              and returns the ID.
     */
    DataType Field::type( void ) {
        if ( _data ) {
            return( _data->type() );
        } else {
            return( DATATYPE_NONE );
        }
    }

    /*
     * FUNCTION:    resolve
     *
     * PARAM:       none
     *
     * RETURNS:     none
     *
     * THROWS:      Error if resolution fails
     *
     * WHATITDOES:  Based on the "_ref_text" (format of "tablename(fieldname)")
     *              the table and field is confirmed to exist otherwise an error
     *              is thrown. Once the field has been confirmed to exist then
     *              a reference (_ref_table) to the Table and the index number
     *              of the referenced field (_ref_index) is stored. A pointer
     *              (_ref_field) to the target field is also stored.
     */
    void Field::resolve( void ) {

        if ( ! _ref_text ) {
            /* no reference defined so nothing to resolve */
            return;
        }

        /* format of the text is "tablename(fieldname)" */

        size_t offset = 0;

        for ( offset = 0 ; _ref_text[ offset ] && ( _ref_text[ offset ] != '(' ) ; offset ++ ) { /* spin */ }

        if ( ! _ref_text[ offset ] ) {
            throw( AppExcept("%s: reference text is invalid '%s'", _name, _ref_text ));
        }

        if (( _ref_table = Table::fetchByName( _ref_text, offset )) == NULL ) {
            std::string name( _ref_text, offset );
            throw( AppExcept("%s: reference text '%s' table not defined", _name, name.c_str() ));
        }

        const char *fname = &_ref_text[ offset + 1 ];

        for ( offset = 0 ; fname[ offset ] && ( fname[ offset ] != ')' ) ; offset ++ ) { /* spin */ }

        if ( fname[ offset ] != ')' ) {
            throw( AppExcept("%s: reference text '%s' field not properly defined", _name, _ref_text ));
        }

        if (( _ref_index = _ref_table->findField( fname, offset )) < 0 ) {
            throw( AppExcept("%s: reference text '%s' field not defined", _name, _ref_text ));
        }

        _ref_field = _ref_table->field( _ref_index );

    }

    /* 
     * FUNCTION:    references
     *
     * PARAMS:      findex_t*   - pointer to storage location for the ref field index
     *
     * RETURNS:     Table *     - pointer to a Table object
     *
     * THROWS:      none
     *
     * WHATITDOES:  If this field references another field then the field index is
     *              stored into *index and a pointer to the Table object is returned.
     *              Otherwise NULL is returned.
     */
    Table *Field::references( findex_t *index ) {
        if ( _ref_table ) {
            *index = _ref_index;
            return( _ref_table );
        } else {
            return( NULL );
        }

    }

    /* FUNCTION:    data
     *
     * PARAMS:      none
     *
     * RETURNS:     Data*   - pointer to the underlying Data object
     *
     * WHATITDOES:  Returns a pointer to the underlying Data object or NULL
     */
    Data *Field::data( void ) {
        return( _data );
    }

}

/* END OF FILE */
