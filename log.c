/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "log.h"

Log logger;

Log::Log() {
    _fname = NULL;
    _fp = stderr;
    _stdout = false;
}

const char *tod( void ) {

    static char text[ 64 ];
    static time_t  now = 0;

    time_t tmp;

    time( &tmp );

    if ( tmp != now ) {
        now = tmp;
        const char *format = "%Y-%m-%d %H:%M:%S";
        strftime( text, sizeof( text ) - 1, format, localtime( &now ));
    }

    return( text );
}

void Log::error( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *tmp = vmprintf( fmt, ap );
    va_end( ap );

    const char *t = tod();

    fprintf( _fp, "[%s] ERROR: %s\n", t, tmp );
    fflush( _fp );
    if ( _stdout ) {
        fprintf( stdout, "[%s] ERROR: %s\n", t, tmp );
    }
    mfree( tmp );
}

void Log::alert( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *tmp = vmprintf( fmt, ap );
    va_end( ap );

    const char *t = tod();

    fprintf( _fp, "[%s] ALERT %s\n", t, tmp );
    fflush( _fp );
    if ( _stdout ) {
        fprintf( stdout, "[%s] ALERT %s\n", t, tmp );
    }
    mfree( tmp );

}

void Log::info( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *tmp = vmprintf( fmt, ap );
    va_end( ap );

    const char *t = tod();

    fprintf( _fp, "[%s] %s\n", t, tmp );
    fflush( _fp );
    if ( _stdout ) {
        fprintf( stdout, "[%s] %s\n", t, tmp );
    }
    mfree( tmp );
}

void Log::debug( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *tmp = vmprintf( fmt, ap );
    va_end( ap );

    const char *t = tod();

    fprintf( _fp, "[%s] DEBUG %s\n", t, tmp );
    fflush( _fp );
    if ( _stdout ) {
        fprintf( stdout, "[%s] DEBUG %s\n", t, tmp );
    }
    mfree( tmp );
}

void Log::configure( YAML::Node map ) {

    if ( map.Type() == YAML::NodeType::Null ) {
        /* nothing to do */
        return;
    } else if ( map.Type() != YAML::NodeType::Map ) {
        logger.error("Logging configuration requires a YAML Map");
        exit( 1 );
    }

    logger._stdout = yamlNodeBoolean( map, "stdout", false );

    if ( map["file"].Type() == YAML::NodeType::Scalar ) {

        if ( logger._fname ) {
            mfree( logger._fname );
            fclose( logger._fp );
            logger._fp = NULL;
        }

        logger._fname = strdup( map["file"].as<std::string>().c_str() );

        if (( logger._fp = fopen( logger._fname, "a+" )) == NULL ) {
            fprintf( stderr, "%s: %s\n", logger._fname, strerror( errno ));
            exit( 1 );
        }
        
        logger.info("writing log to %s", logger._fname );

    }

}

/* END OF FILE */
