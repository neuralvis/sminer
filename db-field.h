/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _DB_FIELD_H_
#define _DB_FIELD_H_

#include    "mylib.h"
#include    "data.h"

namespace DB {

    class Table;

    class Field {

        public:

            static Field *make( Table *table, const char *name, const char *parser, const char *slurm,
                    const char *references, const char *default_value, const char *cast );

            /* return the PG name of this field */
            const char *name( void );

            /* return the PG name length */
            size_t nameLen( void );

            /* return data from this field */
            const char *fetch( void );

            /* return the PG cast string for this field */
            const char *cast( void );

            /* if this field references another field, return the Table reference and field index */
            Table *references( findex_t *index );

            /* return a reference to the Data object */
            Data *data( void );

            /* return the ID of the referenced Data object */
            DataType type( void );

            /* resolve reference connections */
            void resolve( void );

        private:

            Field( Table *table, const char *name, const char *parser, const char *slurm, const char *references,
                    const char *default_value, const char *cast );

            Table           *_table;
            char            *_name;
            size_t          _name_len;
            char            *_cast;
            char            *_parser;
            char            *_slurm;
            char            *_default;

            char            *_ref_text;
            Table           *_ref_table;
            Field           *_ref_field;
            findex_t        _ref_index;

            Data            *_data;
            int             _data_index;

    };

};

#endif
