/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-bytes.h"

DataBytes::DataBytes( const char *field ) : Data( field, DATATYPE_BYTES ) {

}

Data *DataBytes::make( const char *field, const YAML::Node &config ) {
    return( new DataBytes( field ) );
}

void DataBytes::_clear( void ) {
}

int DataBytes::parse( const char *text, size_t len ) {
    
    
    std::string field( text, len );

    try { 

        time_t n = toNumber( field );

        _value = std::to_string( n / 1000000 );
        _set = true;
        return( 0 );

    } catch ( int e ) {

    }

    if ( field[ field.size() - 1 ] == 'M' ) {

        float f;

        if ( sscanf( field.c_str(), "%fM", &f ) == 1 ) {

            _value = std::to_string( (int) f );
            _set = true;

            return( 0 );

        } else {
            printf("failed to obtain floatM\n");
            throw( 2 );
        }

    } else {

        printf("FAIL %s set %s\n", _field, field.c_str() );
        exit( 1 );

    }


#if 0
    char *t = strndup( text, len );
    printf("DataBytes %s is %d\n", t, _value );
    mfree( t );
#endif

    return( 0 );
}

void DataBytes::configure( const YAML::Node &node ) {
    logger.error("XXX");
}


/* END OF FILE */
