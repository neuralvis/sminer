/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _DATA_H_
#define _DATA_H_

#include "mylib.h"

class Data {

    public:

        Data( const char *field, DataType value );
        const char *name( void );
        virtual DataType type( void );

        Data( const char *key, const char *fieldname );
        Data( const char *key, const char *fieldname, const char *cast );

        void clear( void );

        virtual int parse( const char *text, size_t len ) = 0;

        virtual int connect( const char *subtype );

        bool isset( void ) {
            return( _set );
        };

        virtual const char *value( int index ) {
            return( _set ? _value.c_str() : NULL );
        };

        int connections( void ) {
            return( _connections );
        };

        //virtual void configure( const YAML::Node &node );

        /* for iterators */
        virtual bool iterator( void );
        virtual const char *begin( void );
        virtual const char *next( void );

        /* configuration */

        static void configure( const YAML::Node map );
        static Data *connect( const char *type, int *index );

        static Data     **fields;
        static size_t   fields_count;

    protected:

        virtual void _clear( void ) = 0;

        bool            _set;

        std::string     _value;

        char            *_field;

        int             _connections;

    private:

        DataType        _type;

};

typedef std::list<Data*> Data_list_t;
typedef Data_list_t::iterator Data_list_i;

typedef std::map<std::string,Data*> Data_map_t;
typedef Data_map_t::iterator Data_map_i;
typedef std::pair<std::string,Data*> Data_pair_t;
typedef std::pair<Data_map_i,bool> Data_ins_t;

#endif
