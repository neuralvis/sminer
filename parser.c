/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "parser.h"
#include    "slurm.h"
#include    "data.h"
#include    "data-datetime.h"
#include    "data-jobid.h"
#include    "data-tres.h"
#include    "data-nodelist.h"

Parser::Parser() {
    _map = NULL;
    _nfields = 0;
    _index = 0;
    _debug = 0;
    clear();
}

Parser::~Parser() {
    clear();
}

void Parser::reset( void ) {

    _nfields = 0;
    mfree( _map );

    _job_id = NULL;
    _time_submit = NULL;
    _time_start = NULL;
    _time_end = NULL;
    _alloc_tres = NULL;
    _node_list = NULL;

}

bool Parser::isset( void ) {
    return( _nfields > 0 ? true : false );
}

void Parser::label( const char *text, size_t len ) {

    if ( _map == NULL ) {
        _map = (Data**)calloc( sizeof( Data *), Data::fields_count );
    }

    char *name = strndup( text, len );

    for ( size_t index = 0 ; index < Data::fields_count ; index ++ ) {

        if ( strcasecmp( name, Data::fields[ index ]->name() ) != 0 ) {
            continue;
        }

        _map[ _nfields ] = Data::fields[ index ];

        zdebug( _debug & 1, "parser::label( _nfields = %d, field=%s )\n", _nfields, _map[ _nfields ]->name() );

        switch ( _map[ _nfields ]->type() ) {

            case DATATYPE_JOBID:

                _job_id = dynamic_cast<DataJobID*>( _map[ _nfields ] );
                break;

            case DATATYPE_DATETIME:

                if ( strcasecmp( name, "Submit" ) == 0 ) {
                    _time_submit = dynamic_cast<DataDateTime*>(_map[ _nfields ] );
                } else if ( strcasecmp( name, "Start" ) == 0 ) {
                    _time_start = dynamic_cast<DataDateTime*>(_map[ _nfields ] );
                } else if ( strcasecmp( name, "End" ) == 0 ) {
                    _time_end = dynamic_cast<DataDateTime*>(_map[ _nfields ] );
                }
                break;

            case DATATYPE_TRES:

                if ( strcasecmp( name, "AllocTRES" ) == 0 ) {
                    _alloc_tres = dynamic_cast<DataTRES*>( _map[ _nfields ] );
                }
                break;

            case DATATYPE_NODELIST:

                if ( strcasecmp( name, "NodeList" ) == 0 ) {
                    _node_list = dynamic_cast<DataNodeList*>( _map[ _nfields ] );
                }

                break;

            default:
                break;
        }
        
        _nfields ++;
        mfree( name );
        return;

    }

    throw( AppExcept("header label '%s' doesn't match any configured values - abort", name ) );

}

void Parser::data( const char *text, size_t len ) {

    if ( _debug & 2 ) {
        std::string tmp( text, len );
        zdebug( 1, "parser::data( index = %d, text = '%s' )\n", _index, tmp.c_str() );
    }

    if ( _index >= _nfields ) {
        throw( AppExcept("header data injection exceeds label definition length (%d)", _nfields ) );
    } else if ( len > 0 ) {
        /* only fields that have defined data are to be passed 
         * - saves having logic to check arrival of data in
         *   the Data handler
         */
        _map[ _index ]->parse( text, len );
    }

    _index ++;

}

/*
 * FUNCTION:    validate
 *
 * PARAMS:      none
 *
 * RETURNS:     none
 *
 * THROWS:      yes
 *
 * WHATITDOES:  Certain field parsers are mandatory, so check to see each one
 *              of these has been found.
 *
 */
void Parser::validate( void ) {

    struct {
        const char  *label;
        Data        *handler;
    } mandatory[] = {
        { "jobid", _job_id },
        { "time submit", _time_submit },
        { "time start", _time_start },
        { "time end", _time_end },
        { NULL, NULL }
    };

    for ( int ctr = 0 ; mandatory[ ctr ].label ; ctr ++ ) {
        if ( ! mandatory[ ctr ].handler ) {
            throw( AppExcept("header has no configured %s parser", mandatory[ ctr ].label ) );
        }
    }

}

/*
 * FUNCTION:    clear
 *
 * PARAMS:      none
 *
 * RETURNS:     none
 *
 * THROWS:      none
 *
 * WHATITDOSE:  sends a clear request to each of the data handlers.
 *              This needs to be called before parsing each row
 *              to ensure the collected values aren't merged between
 *              rows.
 */
void Parser::clear( void ) {

    for ( findex_t index = 0 ; index < _nfields ; index ++ ) {
        if ( _map[ index ] ) {
            _map[ index ]->clear();
        }
    }
    _index = 0;
}

const char *Parser::job( void ) {

    if ( ! _job_id->isset() ) {
        throw( AppExcept("Job ID of record is not set" ) );
    }

    return( _job_id->job() );
}

bool Parser::shortJob( void ) {

    if ( ! _time_start->isset() ) {
        throw( AppExcept("start time of record is not set" ) );
    } else if ( ! _time_end->isset() ) {
        throw( AppExcept("end time of record is not set" ) );
    }

    if ( ! _time_start->value(0) ) {
        return( false );
    } else if ( ! _time_end->value(0) ) {
        return( false );
    } else {
        return( strcmp( _time_start->value(0), _time_end->value(0) ) == 0 ? true : false );
    }

}

void Parser::line( const char *line, void (Parser::*cb)( const char *text, size_t len ) ) {
    
    size_t      i1 = 0, i2 = 0;
    findex_t    index = 0;

    zdebug( _debug & 4, "LINE: %s\n", line );

    while ( true ) {

        if (( line[i2] == '|')||( line[i2] == '\0' )) {
            size_t l = i2 - i1;
            (this->*cb)( &line[ i1 ], l );
            index ++;
            if ( line[i2] == '\0' ) {
                break;
            }
            i2 += 1;
            i1 = i2;
        } else {
            i2 += 1;
        }
    }

    if ( index != _nfields ) {
        throw( AppExcept("processing input line only %d of %d fields handled", index, _nfields ) );
    }

}

void Parser::debug( int value ) {
    _debug = value;
}

/* END OF FILE */
