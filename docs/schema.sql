
--
-- Slurm Minor Sample PostgreSQL Schema
--
--

CREATE SEQUENCE slurm_job_index START WITH 1 INCREMENT BY 1;

--
-- If importing from multiple clusters it's convenient to separate
-- by cluster name.
--
CREATE TABLE slurm_cluster (
    name VARCHAR PRIMARY KEY NOT NULL
);

--
-- User table
--
CREATE TABLE slurm_user (
    username VARCHAR PRIMARY KEY NOT NULL
);

--
-- Node table
--
CREATE TABLE slurm_node (
    node_name VARCHAR PRIMARY KEY NOT NULL
);

--
-- Job Table
--
CREATE TABLE slurm_job (
    job_index INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('slurm_job_index'),
    job_id VARCHAR NOT NULL,
    username VARCHAR NOT NULL REFERENCES slurm_user( username ),
    account VARCHAR NOT NULL,
    cluster VARCHAR NOT NULL REFERENCES slurm_cluster( name ),
    partition VARCHAR NOT NULL,
    state VARCHAR NOT NULL,
    qos VARCHAR,
    reservation VARCHAR,
    priority INT,
    time_submit TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    time_start TIMESTAMP WITHOUT TIME ZONE,
    time_end TIMESTAMP WITHOUT TIME ZONE,
    tres_cpu INT NOT NULL,
    tres_gpu INT NOT NULL,
    tres_memory INT NOT NULL,
    timelimit INT NOT NULL,
    nodes INT,
    exitcode VARCHAR,
    -- as slurm can re-use a JobID, force a primary key with jobid and submit time
    UNIQUE( job_id, time_submit )
);

CREATE TABLE slurm_step (
    step_index INT NOT NULL PRIMARY KEY DEFAULT nextval('slurm_job_index'),
    job_index INT NOT NULL REFERENCES slurm_job( job_index ),
    job_id VARCHAR NOT NULL,
    time_submit TIMESTAMP WITHOUT TIME ZONE,
    time_start TIMESTAMP WITHOUT TIME ZONE,
    time_end TIMESTAMP WITHOUT TIME ZONE,
    cputime BIGINT,
    state VARCHAR,
    alloc_cpu BIGINT,
    alloc_mem BIGINT,
    alloc_node BIGINT,
    alloc_gpu BIGINT,
    cpu_total BIGINT,
    cpu_user BIGINT,
    cpu_system BIGINT,
    avevmsize BIGINT,
    averss BIGINT,
    elapsed BIGINT,
    exitcode VARCHAR,
    maxdiskread BIGINT,
    maxdiskreadnode VARCHAR REFERENCES slurm_node( node_name ),
    maxdiskwrite BIGINT,
    maxdiskwritenode VARCHAR REFERENCES slurm_node( node_name ),
    maxrss BIGINT,
    maxrssnode VARCHAR REFERENCES slurm_node( node_name ),
    maxvmsize BIGINT,
    maxvmsizenode VARCHAR REFERENCES slurm_node( node_name ),
    mincpu BIGINT,
    mincpunode VARCHAR REFERENCES slurm_node( node_name ),
    UNIQUE( job_index, job_id, time_start )
);

CREATE TABLE slurm_job_node_link (
    job_index INT NOT NULL REFERENCES slurm_job( job_index ),
    node_name VARCHAR NOT NULL REFERENCES slurm_node( node_name ),
    UNIQUE( job_index, node_name )
);

CREATE TABLE slurm_step_node_link (
    step_index INT NOT NULL REFERENCES slurm_step( step_index ),
    node_name VARCHAR NOT NULL REFERENCES slurm_node( node_name ),
    UNIQUE( step_index, node_name )
);

-- END OF FILE
