/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _MYLIB_H_
#define _MYLIB_H_

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <strings.h>
#include <sys/time.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <assert.h>
#include <getopt.h>

#include <libpq-fe.h>
#include <postgres_ext.h>

#include <yaml-cpp/yaml.h>

#include <list>
#include <map>
#include <string>

char *mprintf( const char *fmt, ... );
char *vmprintf( const char *fmt, va_list ap );

char *xstrdup( const char *text );
char *xstrndup( const char *text, size_t len );
int xstrcmp( const char *s1, const char *s2 );
int xstrcasecmp( const char *s1, const char *s2 );
size_t xstrlen( const char *s );
int strcasecmp( std::string &s1, const char *s2 );

size_t toMBytes( const char *text, size_t len );

bool contains( const char *string, const char *substring );

time_t rewindToMidnight( time_t value );

#define mfree( x ) if( x ) { free( x ); x = NULL; }

typedef std::map<std::string,std::string> string_map_t;
typedef string_map_t::iterator string_map_i;
typedef std::pair<std::string,std::string> string_pair_t;

time_t toNumber( const std::string &text );
time_t toNumber( const char *text, size_t len );

typedef std::list<std::string> string_list_t;
typedef string_list_t::iterator string_list_i;

typedef std::list<int> int_list_t;
typedef int_list_t::iterator int_list_i;

void nodeExpand( string_list_t &nodes, const std::string &text );

const char *xstrnchr( const char *str, size_t len, char c );

bool yamlNodeBoolean( const YAML::Node &node, const char *key, bool def );

void _zdebug( const char *filename, int line, const char *func, const char *fmt, ... );

extern int debug;

typedef enum {
    DATATYPE_NONE = 0,
    DATATYPE_INT,
    DATATYPE_STRING,
    DATATYPE_NODE,
    DATATYPE_NODELIST,
    DATATYPE_DATETIME,
    DATATYPE_ELAPSED,
    DATATYPE_TRES,
    DATATYPE_STATE,
    DATATYPE_JOBID,
    DATATYPE_BYTES
} DataType;

typedef int findex_t;

#define zdebug( v, x... ) \
    if ( v ) { \
        _zdebug( __FILE__, __LINE__, __func__, x ); \
    } 
  
#include "app-except.h"
#include "app-warning.h"
#include "app-fatal.h"
#include "app-param.h"
#include "log.h"
#include "build-definitions.h"

#endif
