/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "config.h"
#include    "db-table.h"
#include    "slurm.h"
#include    "data.h"
#include    "log.h"
#include    "postgres.h"

Config config;

Config::Config() {
}

int Config::load( const char *filename ) {

    try {

        YAML::Node data = YAML::LoadFile( filename );

        if ( ! data.IsMap() ) {
            throw( AppExcept("Incorrectly formatted YAML file - expected a top level Map") );
        }

        try {
            Log::configure( data["logging"] );
        } catch ( const AppExcept &e ) {
            throw( AppExcept( "log configuration: %s", e.what() ));
        }

        try {
            pg.configure( data["postgres"] );
        } catch ( const AppExcept &e ) {
            throw( AppExcept( "postgres configuration: %s", e.what() ));
        }

        try {
            Data::configure( data["sacct"] );
        } catch ( const AppExcept &e ) {
            throw( AppExcept( "sacct configuration: %s", e.what() ));
        } catch ( const AppFatal &e ) {
            throw( AppFatal( "sacct configuration: %s", e.what() ));
        }

        try { 
            DB::Table::configure( data["tables"] );
            DB::Table::resolve();
        } catch ( const AppExcept &e ) {
            throw( AppExcept( "table configuration: %s", e.what() ));
        }

    } catch ( YAML::ParserException &e ) {

        printf("%s: YAML parsing error: %s (line %d column %d)\n",
                filename, e.msg.c_str(),
                e.mark.line, e.mark.column );
        exit( 1 );

    } catch ( YAML::BadFile &e ) {

        printf("Unable to read configuration from %s: %s\n",
                filename, strerror( errno )); //e.msg.c_str() );
        exit( 1 );

    } catch ( YAML::BadSubscript &e ) {

        printf("%s: Unable to load YAML file: %s\n",
                filename, e.msg.c_str() );
        exit( 1 );

    }

    return( 0 );
}

/* END OF FILE */
