/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _DB_TABLE_H_
#define _DB_TABLE_H_

#include    "mylib.h"
#include    "data.h"
#include    "stats.h"
#include    "db-row.h"

class Stats;
class Query;

namespace DB {

    class Field;
    class Table;
    class Row;

    typedef std::map<std::string,Table*> Table_map_t;
    typedef Table_map_t::iterator Table_map_i;
    typedef std::pair<std::string,Table*> Table_pair_t;
    typedef std::pair<Table_map_i,bool> Table_ins_t;

    typedef enum {
        DB_COMMAND_OK = 0,
        DB_FATAL,
        DB_DUPLICATE
    } DBCODE;

    typedef struct {
        findex_t    index;
    } TableIndex;

    typedef struct {
        const char  *value;
        bool        isset;
    } Value_t;

    class Table {

        public:

            /* configure DB tables */
            static void configure( YAML::Node config );

            /* verify DB table dependencies */
            static void resolve( void );

            /* preload DB table data */
            void preload( const char *time_start, const char *time_end );

            /* inject SACCT data from slurm */
            const char *inject( void );

            static void stats( void );

            const char *slurm( void );

            static Table *fetch( const char *type );
            static Table *fetchByName( const char *type );
            static Table *fetchByName( const char *type, size_t len );

            Table( const char *type, const char *name, bool preload );

            void name( const char *text );
            const char *name( void );
            const char *type( void );

            const char *table_name( void );

            static void clear( void );

            void describe( void );
            int findField( const char *field_name );
            int findField( const char *field_name, size_t len );
            Field *field( findex_t index );
            void set( int index, const char *value );
            void debug( bool value );

            Row *last( void );

        private:

            Row *rowFindOrAllocate( std::string &index, bool *found );
            bool injectAction( Value_t *values );

            void reset( void );
            void build( YAML::Node &fields );
            void indexes( YAML::Node node );
            void indexAdd( const YAML::Node &node );
            void constructIndex( std::string &index, Row *row );
            void constructIndexFromSlurm( std::string &index, const char *(Field::*cb)( void ) );
            void update( Row *row );
            void insert( Row *row );
            void select( Row *row );
            void build( Query &query, Value_t *values );
            void select( Query &query );
            bool makeJoinedQuery( Query &query );
            bool injectValue( findex_t value_index );
            void insertByKey( findex_t index, const char *value );

            bool            _preload;

            /* the fields connected to this DB Table */
            Field           **_fields;
            findex_t        _fields_count;

            /* the values to be used when inserting / updating a DB row */
            Value_t         *_values;

            /* the indexes of this table - mapped to _fields */
            findex_t        *_indexes;
            int             _indexes_count;

            /* the "DB Primary Key" field number */
            findex_t        _pkey;

            /* the last DB row managed for this table */
            Row             *_last;

            /* debug flag */
            bool            _debug;

            /* A std::map of DB::Row indexed by the defined index (compound) */
            Row_map_t    _rows;

            /* the table type (ie: job,step, etc) */
            char            *_type;

            /* the DB Table name (including length of the name) */
            char            *_pg_table_name;
            size_t          _pg_table_name_len;

            /* stats accumulator for DB operations */
            Stats           _stats;

            /* A std::map of all defined DB::Tables */
            static Table_map_t  tables;

    };

};

#endif
