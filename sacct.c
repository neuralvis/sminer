/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "sacct.h"
#include    "mylib.h"
#include    "timer.h"
#include    "data-jobid.h"
#include    "data-datetime.h"
#include    "data-nodelist.h"
#include    "data-tres.h"
#include    "query.h"
#include    "postgres.h"
#include    "db-table.h"
#include    "stats.h"
#include    "node.h"
#include    "config.h"
#include    "slurm.h"
#include    "log.h"

/* 'scan' the last <n> days */
time_t   SACCT::scan = 0;

/* scan interval is <n> days */
time_t   SACCT::duration = 1;

/* start time of scan */
time_t   SACCT::timeStart = 0;

/* end time of scan - if not set use 'time()' */
time_t   SACCT::timeEnd = 0;

SACCT::SACCT() {
    _period_begin = 0;
    _period_end = 0;
}

int SACCT::run( void ) {

    Timer run;

    run.start();

    _job = DB::Table::fetch( "job" );
    //_job->debug( true);
    _step = DB::Table::fetch( "step" );
    //_step->debug( true );
    _step_node_link = DB::Table::fetch( "step_node_link" );
    //_step_node_link->debug( true );
    _job_node_link = DB::Table::fetch( "job_node_link" );
    //_job_node_link->debug( true );

    struct tm tm;
    bzero( &tm, sizeof( tm ));

    tm.tm_year = 118;
    tm.tm_mon = 0;
    tm.tm_mday = 1;

    /*
    tm.tm_year = 121;
    tm.tm_mon = 0;
    tm.tm_mday = 31;
    */

    if ( timeStart && scan ) {
        throw( AppFatal("Only one of '--start <t>' or '--scan <d>' can be specified. Use '--help' for more information" ) );
    }

    if ( ! timeStart ) {
        timeStart = rewindToMidnight(  time(NULL) - (( scan -1 ) * 86400 ) );
    }

    time_t interval = duration * 86400;

    _period_begin = timeStart;

    for ( ; true ; _period_begin += interval ) {

        if ( timeEnd && ( _period_begin > timeEnd ) ) {
            /* an end time is specified, and it has been exceeded */
            break;
        } else if ( _period_begin >= time( NULL ) ) {
            /* no end time, so break when exceeding the current time */
            break;
        }

        _period_end = _period_begin + interval;

        struct tm *tm = localtime( &_period_begin );

        snprintf( _time_text_start, sizeof( _time_text_start ), "%04d-%02d-%02d",
                tm->tm_year + 1900, 
                tm->tm_mon + 1,
                tm->tm_mday );

        tm = localtime( &_period_end );

        snprintf( _time_text_end, sizeof( _time_text_end ), "%04d-%02d-%02d",
                tm->tm_year + 1900, 
                tm->tm_mon + 1,
                tm->tm_mday );

        logger.info("processing period %s to %s", _time_text_start, _time_text_end );

        _job->preload( _time_text_start, _time_text_end );
        if ( _job_node_link ) { 
            _job_node_link->preload( _time_text_start, _time_text_end );
        }
        if ( _step ) {

            _step->preload( _time_text_start, _time_text_end );

            if ( _step_node_link ) { 
                _step_node_link->preload( _time_text_start, _time_text_end );
            }
        }

    _job_node_link = DB::Table::fetch( "job_node_link" );

        char *cmd = mprintf("/usr/sbin/sacct -P%s --noconvert -S %s -E %s -D --allusers --format=%s",
                _step ? "" : " -X",
                _time_text_start, _time_text_end, Slurm::sacct_fields() );

#if 0
        logger.debug("cmd = %s", cmd );
#endif

        logger.info("executing \"sacct -S %s -E %s ...\"", _time_text_start, _time_text_end );

        FILE *fp;

        Timer timer;

        timer.start();

        Timer first;

        first.start();

        if (( fp = popen( cmd, "r" )) == NULL ) {
            perror("popen");
            exit(1);
        }

        char line[ 65536 ];
        int line_ctr = 0;

        _job_index = NULL;

        while ( fgets( line, sizeof( line ), fp ) ) {

            if ( first.running() ) {
                first.stop();
                logger.info("first sacct record available in %.2lf seconds", first.seconds() );
            }

            line_ctr ++;
            size_t len = strlen( line );
            while (( len > 0 )&&( isspace( line[ len - 1 ] ))) {
                line[ -- len ] = '\0';
            }

            if ( ! _parser.isset() ) {
                /* process header */

                _parser.reset();

                try {
                    _parser.line( line, &Parser::label );
                    _parser.validate();
                } catch ( AppExcept &e ) {
                    logger.error("sacct[%d]: %s", line_ctr, line );
                    logger.error("sacct[%d]: %s", line_ctr, e.what() );
                    exit( 1 );
                }

            } else {
                /* process data */

                _parser.clear();

                /* and parse the data line */
                try {

                    _parser.line( line, &Parser::data );

                    if ( ! _parser.job() ) {

                        /* process on the job table */

                        _job->inject();
                        _job_node_link->inject();

                    } else {

                        _step->inject();
                        _step_node_link->inject();

                    }

                } catch ( AppExcept &e ) {
                    logger.error("sacct[%d]: %s", line_ctr, line );
                    logger.error("sacct[%d]: %s", line_ctr, e.what() );
                    exit( 1 );
                } catch ( AppWarning &e ) {
                    logger.alert("sacct[%d]: %s", line_ctr, line );
                    logger.alert("sacct[%d]: %s", line_ctr, e.what() );
                } catch ( AppFatal &e ) {
                    logger.error("sacct[%d]: %s", line_ctr, line );
                    logger.error("sacct[%d]: %s", line_ctr, e.what() );
                    exit( 1 );
                }
            }

        }

        /* reset the parser state, ready for next time */
        _parser.reset();

        if ( pclose( fp ) < 0 ) {
            logger.error( "failure closing pipe: %s", strerror( errno ));
            exit( 1 );
        }

        timer.stop();

        logger.info("  %6d lines in %11.3lf seconds (%9.2lf rps)",
                line_ctr,
                timer.seconds(),
                (double)line_ctr / timer.seconds() );

        DB::Table::stats();
        DB::Table::clear();

        mfree( cmd );

    }

    run.stop();

    logger.info("End of run in %.6lf seconds", run.seconds() );

    return( 0 );
}

/*
 * FUNCTION:    setDayCount
 *
 * PARAMS:      const char * - days to cover
 *
 * RETURNS:     none
 *
 * THROWS:      AppParam if not an integer
 */
void SACCT::setDayCount( const char *value, void *ptr ) {

    for ( const char *ptr = value ; *ptr ; ptr ++ ) {
        if ( ! isdigit( *ptr ) ) {
            throw( AppParam("value '%s' is invalid - must only contain digits", value ) );
        }
    }

    time_t number = atoi( value );

    if ( number <= 0 ) {
        throw( AppParam("%d is too small - day count must be > 0", number ) );
    } else if ( number > 31 ) {
        throw( AppParam("%d is too large - day count be <= 31", number ) );
    }

    /* store the day count */
    *(time_t*)ptr = number;

}

/*
 * FUNCTION:    setTime
 *
 * PARAMS:      const char * - start timestamp
 *
 * RETURNS:     none
 *
 * THROWS:      AppParam if value not converted
 *
 * DESCRIPTION: Valid formats are:
 *
 *              YYYY-MM-DD
 */
void SACCT::setTime( const char *value, void *ptr ) {

    const char *formats[] = {
        "%Y-%m-%d",
        NULL
    };

    for ( int index = 0 ; formats[ index ] ; index ++ ) {

        struct tm tm;
        bzero( &tm, sizeof( tm ));

        const char *rv = strptime( value, formats[ index ], &tm );

        if (( rv )&&( rv[0] == '\0' )) {
            *(time_t*)ptr = mktime( &tm );
            return;
        }

    }

    throw( AppParam("unable to convert '%s' into a timestamp", value ) );

}

/* END OF FILE */
