/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "timer.h"

Timer::Timer() {
    bzero( &_begin, sizeof( _begin ) );
    bzero( &_end, sizeof( _end ) );
    bzero( &_diff, sizeof( _diff ) );
}

void Timer::start( void ) {
    gettimeofday( &_begin, NULL );
    _running = true;
}

void Timer::stop( void ) {
    gettimeofday( &_end, NULL );
    _running = false;

    _diff.tv_sec = _end.tv_sec - _begin.tv_sec;
    if ( _begin.tv_usec < _end.tv_usec ) {
        _diff.tv_usec = _end.tv_usec - _begin.tv_usec;
    } else {
        _diff.tv_sec -= 1;
        _diff.tv_usec = 1000000 + _end.tv_usec - _begin.tv_usec;
    }



}

double Timer::seconds( void ) {

    return( (double)_diff.tv_sec +
            ( (double)_diff.tv_usec ) / 1000000.0 );

}

time_t Timer::sec( void ) {
    return( _diff.tv_sec );
}

time_t Timer::usec( void ) {
    return( _diff.tv_usec );
}

bool Timer::running( void ) {
    return( _running );
}

/* END OF FILE */
