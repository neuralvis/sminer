/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "slurm.h"

#include    "data.h"
#include    "log.h"

namespace Slurm {
    
    /* the fields used for extraction by sacct */
    char           *_sacct_fields = NULL;
    static size_t  sacct_field_len = 0;

    const char *sacct_fields( void ) {

        if ( ! _sacct_fields ) {

            size_t used = 0;

            for ( size_t index = 0 ; index < Data::fields_count ; index ++ ) {

                if ( ! Data::fields[ index ] ) {
                    /* skip this field */
                    continue;
                } else if ( Data::fields[ index ]->connections() == 0 ) {
                    /* no connections - skip */
                    continue;
                }

                /* add the field onto the field list */
                size_t len = strlen( Data::fields[ index ]->name() );

                _sacct_fields = (char*)realloc( _sacct_fields, sizeof( char ) * ( sacct_field_len + len + 2 ));

                if ( used > 0 ) {
                    _sacct_fields[ sacct_field_len++ ] = ',';
                }

                strcpy( &_sacct_fields[ sacct_field_len ], Data::fields[ index ]->name() );
                sacct_field_len += len;

                used ++;

            }

        }

        return( _sacct_fields );
    }

}

/* END OF FILE */
