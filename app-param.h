/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _APP_PARAM_
#define _APP_PARAM_

#include    <exception>

class AppParam : public std::exception {
    
    public:
        
        explicit AppParam( const char *fmt, ... ) {
            va_list ap;
            va_start( ap, fmt );
            char *tmp = vmprintf( fmt, ap );
            va_end( ap );
            _message = tmp;
            mfree( tmp );
        };

        explicit AppParam( const std::string &msg ) : 
            _message( msg ) {}

        virtual ~AppParam() throw () {}

        virtual const char *what() const throw() {
            return( _message.c_str() );
        }

    protected:

        std::string     _message;

};

#endif
