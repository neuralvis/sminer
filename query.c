/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "query.h"

#undef DEBUG

Query::Query() {
    _nParams = 0;
    _paramValues = NULL;
    _paramLengths = NULL;
    //_paramFormats = NULL;
}

Query::~Query() {
    clear();
}

void Query::clear( void ) {

    sql.clear();

    mfree( _paramLengths );

    for ( int ctr = 0 ; ctr < _nParams ; ctr ++ ) {
        mfree( _paramValues[ ctr ] );
    }

    mfree( _paramValues );

    _nParams = 0;

}

int Query::_addParameter( void ) {

    int index = _nParams;

    _nParams ++;

    _paramValues = (char**)realloc( _paramValues, sizeof( *_paramValues ) * _nParams );
    _paramValues[ index ] = NULL;

    return( _nParams - 1 );
}

void Query::parameter( const std::string &field_value ) {
    parameter( field_value.c_str() );
}

void Query::parameter( const char *field_value ) { // const char *cast ) {
    int index = _addParameter();
    _paramValues[ index ] = strdup( field_value );
    //printf("PARAM[%d] = '%s'\n", index, field_value );
}

PGresult *Query::execute( PGconn *conn ) {

    if ( debug ) {
        dump( __FILE__, __LINE__ );
    }

    return( PQexecParams( conn,
                sql.c_str(),
                _nParams,
                NULL,
                _paramValues,
                NULL,
                NULL,
                0 ));

}

void Query::dump( const char *filename, int line ) {

    logger.error("Query::dump( \"%s\", %d )", filename, line );
    logger.error("SQL: %s", sql.c_str() );
    for ( int i = 0 ; i < _nParams ; i ++ ) {
        logger.error("  PARAM $%d = %s", i + 1, _paramValues[ i ] );
    }

}

void Query::msql( const char *fmt, ... ) {
    va_list ap;
    va_start( ap, fmt );
    char *tmp = vmprintf( fmt, ap );
    va_end( ap );
    sql = tmp;
    mfree( tmp );
}

/* END OF FILE */
