/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "sacct.h"
#include    "config.h"
#include    "mylib.h"
#include    "args.h"
#include    "sacct.h"

Args    parser;

void version( void );

int main( int argc, char *argv[] ) {

    SACCT   sacct;

    parser.desc("Slurm Miner - a tool for transferring \"sacct\" records into a PostgreSQL database");

    parser.addRequiredArgument( "config" )
        .defaultValue( "/etc/sminer.yaml" )
        .desc("Configuration file" );

    parser.addRequiredArgument( "duration", 'd' )
        .defaultValue("1")
        .handler( &SACCT::setDayCount, &SACCT::duration )
        .desc("Length of period in days for records to be retrieved (format: integer)" );

    parser.addRequiredArgument( "start", 's' )
        .handler( &SACCT::setTime, &SACCT::timeStart )
        .desc( "Timestamp for starting scan (format: YYYY-MM-DD)" );

    parser.addRequiredArgument( "end", 'e' )
        .handler( &SACCT::setTime, &SACCT::timeEnd )
        .desc( "Timestamp for ending the scan (format: YYYY-MM-DD)" );

    parser.addRequiredArgument( "scan" )
        .handler( &SACCT::setDayCount, &SACCT::scan )
        .desc( "Number of days preceding now to scan (format: integer)" );

    parser.addRequiredArgument( "logfile", 'l' )
        .defaultValue( "/var/log/sminer.log" )
        .desc( "Log file" );

    parser.addArgument( "version", 'v' )
        .action( &version )
        .desc( "Display version, copyright and license" );

    parser.parse_args( argc, argv );

    const char *filename = parser["config"];

    try {
        config.load( filename );
    } catch ( AppExcept &e ) {
        fprintf( stderr, "%s: %s\n", filename, e.what() );
        exit( 1 );
    } catch ( AppFatal &e ) {
        fprintf( stderr, "%s: %s\n", filename, e.what() );
        exit( 1 );
    }

    try {
        sacct.run();
    } catch ( AppFatal &e ) {
        fprintf( stderr, "sacct-runner: %s\n", e.what() );
        exit( 1 );
    }


}

void version( void ) {

    printf("%s: %s\n", PROG_NAME, PROG_VERSION );
    printf("Author: %s\n", PROG_AUTHOR );
    printf("Built: %s\n", PROG_BUILD );
    if ( PROG_COPYRIGHT ) {
        printf("Copyright: %s\n", PROG_COPYRIGHT );
    }
    if ( PROG_LICENSE ) {
        printf("License: %s\n", PROG_LICENSE );
    }

    exit( 0 );
}

/* END OF FILE */
