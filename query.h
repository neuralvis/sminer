/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _QUERY_H_
#define _QUERY_H_

#include    "mylib.h"

#include    <libpq-fe.h>

class Query {

    public:

        Query();
        ~Query();

        void clear( void );
        void parameter( const char *field_value );
        void parameter( const std::string &field_value );

        PGresult *results( void );

        void msql( const char *fmt, ... );
        std::string sql;

        void dump( const char *filename, int line );

        /* called from Postgres */
        PGresult *execute( PGconn *conn );

    private:
        
        int     _addParameter( void );

        int     _nParams;

        char    **_paramValues;
        int     *_paramLengths;
        //int     *_paramFormats;

        std::string _table;

};

#endif
