/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "data-int.h"

Data *DataInt::make( const char *field, const YAML::Node &config ) {
    return( new DataInt( field, config ) );
}

DataInt::DataInt( const char *field, const YAML::Node &config ) : Data( field, DATATYPE_INT ) {

    /* process extra configuration options */

    if ( config.IsNull() ) {

        /* no configuration YAML? return . . */
        return;

    } else if ( ! config.IsMap() ) {

        /* otherwise the YAML::Node must be a Map */
        throw( AppFatal("If not a Scalar YAML::Node expected to be a Map") );

    }

    /* iterate over the keys */
    for ( YAML::const_iterator i = config.begin() ; i != config.end() ; i ++ ) {

        const char *key = i->first.as<std::string>().c_str();

        if ( ! i->second.IsScalar() ) {
            throw( AppFatal("YAML configuration for '%s' must be a Scalar", key ) );
        }

        const char *value = i->second.as<std::string>().c_str();
            
        if ( strcasecmp( "calc", key ) == 0 ) {
            _rpn.compile( value );
        }

    }

}

void DataInt::_clear( void ) {
}

int DataInt::parse( const char *text, size_t len ) {

    rpn_t number = 0;

    for ( size_t i = 0 ; i < len ; i ++ ) {
        if ( ! isdigit( text[ i ] ) ) {
            std::string tmp( text, len );

            if ( tmp == "UNLIMITED" ) {
                /* ignore this */
                return( 0 );
            }

            throw( AppWarning( "%s: error passing '%s'", _field, tmp.c_str() ) );
        }

        number = ( number * 10 ) + ( text[ i ] - '0' );

    }

    /* if the _rpn processor has been setup, then process the number */
    if ( _rpn.defined() ) {
        number = _rpn.run( number );
    }

#if 0
    if ( number > 2147483647 ) {
        throw( AppWarning( "%s: integer %ld exceeds size range'", _field, number ) );
    }
#endif

    _value = std::to_string( (int64_t)number );
    _set = true;

    return( 0 );
}

/* END OF FILE */
