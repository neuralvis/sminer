/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _DATA_NODE_H_
#define _DATA_NODE_H_

#include    "data.h"

class Node {

};

typedef std::map<std::string,Node> Node_map_t;
typedef Node_map_t::iterator Node_map_i;

class DataNode : public Data {

    public:

        static Data *make( const char *field, const YAML::Node &config );
        
        DataNode( const char *field );

        int parse( const char *text, size_t len );

        static Node_map_t nodes;

    protected:

        void _clear( void );


};

#endif
