/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#ifndef _MY_TYPES_H_
#define _MY_TYPES_H_

class AutoIntPtr {

    public:

        AutoIntPtr( int size ) {
            data = (int*)calloc( sizeof( int ), size );
        }

        ~AutoIntPtr() {
            mfree( data );
        }

        int *data;

    private:

};

class AutoCharPtr {

    public:

        AutoCharPtr( size_t size ) {
            _size = size;
            data = (char**)calloc( sizeof( char* ), size );
        }

        ~AutoCharPtr() {
            for ( size_t ctr = 0 ; ctr < _size ; ctr ++ ) {
                mfree( data[ ctr ] );
            }
            mfree( data );
        }

        char **data;

    private:

        size_t _size;

};

#endif
