/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "node.h"
#include    "query.h"
#include    "postgres.h"
#include    "stats.h"

Node_map_t Node::nodes;

Node::Node() {

}

bool Node::make( const std::string &name ) {

    Node_map_i i;

    if (( i = nodes.find( name )) != nodes.end() ) {
        return( true );
    }

    /* first try a select */

    Query query;

    query.sql = "SELECT * FROM ibex_node WHERE node_name = $1::VARCHAR";
    query.parameter( name );

    int status;
    
    if ( pg.execute( query ) < 0 ) {
        printf("catastrophic Postgres error\n");
        exit( 1 );
    }

    switch (( status = pg.resultStatus() )) {
        case PGRES_TUPLES_OK:
            if ( pg.ntuples() > 0 ) {
                nodes.insert( Node_pair_t( name, Node() ));
                table_node.select ++;
                return( true );
            }
            break;
        default:
            printf("PG status = %d\n", status );
            exit( 1 );
            break;
    }

    query.clear();

#if 0
    printf("Need to insert %s\n", name.c_str() );
#endif

    query.sql = "INSERT INTO ibex_node ( node_name ) VALUES ( $1 )";
    query.parameter( name );

    if ( pg.execute( query ) < 0 ) {
        printf("catastrophic Postgres error\n");
        exit( 1 );
    }

    switch (( status = pg.resultStatus() )) {
        case PGRES_COMMAND_OK:
            nodes.insert( Node_pair_t( name, Node() ));
            table_node.insert ++;
            return( true );
            break;
        default:
            printf("PG status = %d\n", status );
            printf("ERROR %s\n", pg.errorMessage() );
            exit( 1 );
            break;
    }

    exit( 1 );

}

/* END OF FILE */
