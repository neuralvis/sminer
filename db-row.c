/***********************************************************************
 *  sminer - Synchronise a database with Slurm sacct records
 ***********************************************************************
 * Copyright (C) 2021 Greg Wickham <greg@wickham.me>
 *
 * This file is part of sminer.
 *
 *  sminer is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   sminer is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with sminer.  If not, see <https://www.gnu.org/licenses/>.
 **********************************************************************/

#include    "db-row.h"

namespace DB {

    /*
     * FUNCTION:    valueDifferent
     *
     * PARAMS:      const char * - string #1
     *              const char * - string #2
     *
     * RETURNS:     bool - true for different; false for the same
     */
    static bool valueDifferent( const char *p1, const char *p2 ) {

        if (( p1 )&&( p2 )) {
            /* perform string comparison */
            return( strcmp( p1, p2 ) == 0 ? false : true );
        } else if (( ! p1 )&&( ! p2 ) ) {
            /* both values are not set so no change */
            return( false );
        } else {
            /* one value is set - change */
            return( true );
        }

    }

    /*
     * FUNCTION:    Constructor
     *
     * PARAMS:      findex_t - number of values that can be stored
     */
    Row::Row( findex_t nfields ) {

        _nfields = nfields;

        _values = (char**)calloc( sizeof( char *), _nfields );
        _dirty = (bool*)calloc( sizeof(bool), _nfields );
        _set = (bool*)calloc( sizeof(bool), _nfields );

    }

    Row::~Row() {

        for ( findex_t ctr = 0 ; ctr < _nfields ; ctr ++ ) {
            mfree( _values[ ctr ] );
        }

        mfree( _values );
        mfree( _dirty );
        mfree( _set );

    }

    /*
     * FUNCTION:    set
     *
     * PARAMS:      findex_t - field to set
     *              const char * - value to set
     *
     *  WHATITDOES: sets the value of findex_t
     *              If the value is NULL then
     *              NULL is stored; The dirty
     *              flag is cleared
     *
     *              If the dirty flag is set and
     *              the value being stored is the
     *              same then the dirty flag is
     *              cleared.
     */
    void Row::set( findex_t field, const char *text ) {

#if 0
        printf("Row::set( %d, text = %s ) value=%s dirty=%d set=%d\n",
                field, text, _values[ field ], _dirty[ field ], _set[ field ] );
#endif

        if ( ! _set[ field ] ) {

            /* if the field isn't set then save the value */
            _values[ field ] = xstrdup( text );
            _dirty[ field ] = false;
            _set[ field ] = true;

        } else if ( valueDifferent( text, _values[ field ] ) ) {

            /* values are different - ensure dirty flag is set */
            _dirty[ field ] = true;

        } else {
            /* values are the same so clear the dirty flag */
            _dirty[ field ] = false;
        }

    }

    /*
     * FUNCTION:    update
     *
     * PARAMS:      findex_t - field to set
     *              const char * - value to set
     *
     * WHATITDOES:  sets the value of findex_t;
     *              If the value is NULL then
     *              NULL is stored; if the value
     *              changes then the dirty flag
     *              is set
     */
    void Row::update( findex_t field, const char *text ) {

        if ( ! _set[ field ] ) {

            /* no value yet store - so save it ( and mark as dirty ) */
            _values[ field ] = xstrdup( text );
            _set[ field ] = true;
            _dirty[ field ] = true;

        } else if ( valueDifferent( text, _values[ field ] ) ) {

            /* values are different - ensure dirty flag is set */
            mfree( _values[ field ] );
            _values[ field ] = xstrdup( text );
            _dirty[ field ] = true;

        } else {

            /* values are the same - no change */

        }

    }

    /*
     * FUNCTION:    fetch
     *
     * PARAMS:      findex_t - field to fetch
     *
     * WHATITDOES:  returns a value for the field index
     */
    const char *Row::fetch( findex_t field ) {
        return( _values[ field ] );
    }

    /*
     * FUNCTION:    dirty
     *
     * PARAMS:      none
     *
     * WHATITDOES:  returns boolean TRUE if any field is dirty,
     *              otherwise FALSE.
     */
    bool Row::dirty( void ) {

        for ( findex_t field = 0 ; field < _nfields ; field ++ ) {
            if ( _dirty[ field ] ) {
                return( true );
            }
        }

        return( false );
    }

    /*
     * FUNCTION:    dirty
     *
     * PARAMS:      findex_t - field to fetch
     *
     * WHATITDOES:  returns boolean TRUE / FALSE depending
     *              upon whether the field is dirty or not.
     */
    bool Row::dirty( findex_t field ) {
        return( _dirty[ field ] );
    }

    /*
     * FUNCTION:    sync
     *
     * PARAMS:      none
     *
     * RETURNS:     none
     *
     * WHATITDOES:  clears the dirty flag of all fields
     *
     * WHY?         Use this after a row has been written to
     *              the database.
     */

    void Row::sync( void ) {

        for ( findex_t field = 0 ; field < _nfields ; field ++ ) {
            _dirty[ field ] = false;
        }

    }

    bool Row::isset( findex_t field ) {
        return( _set[ field ] );
    }

    void Row::dump( void ) {
        printf("Row dump\n");
        for ( findex_t field = 0 ; field < _nfields ; field ++ ) {
            printf("field[%d] set=%d dirty=%d value='%s'\n",
                    field, _set[ field ], _dirty[ field ], _values[ field ] );
        }

    }

}

/* END OF FILE */
